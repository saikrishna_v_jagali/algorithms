#fibonacci using memozisation method, where result is calculated from top-down approach using lookup

def fibonacci(n):
    if(lookup[n] == None):  
        if(n <= 1):
            lookup[n] = n
        else:
            lookup[n] = fibonacci(n-1) + fibonacci(n-2)
            
    return lookup[n]

num = 3
lookup = [None] * (num+1)

fibonacci(num)

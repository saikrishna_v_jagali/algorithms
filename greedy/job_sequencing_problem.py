'''
* arrange the jobs in decreasing order of profits
* While sequencing a first job always assign the job to last available slot
* for the next possible jobs, start from their deadline time and trace the slot backward for the availabilty.
* if slot is available alocate the slot for that job and add up the profit
'''
def sequenceJobs(jobs,n): 
    slots = [False] * n
    jobs_sequence  = [None]  * n
    profit = 0
    for i in jobs:
        for j in range(i[1]-1,-1,-1):   #start from last deadline of a job
            if(not slots[j]):
                slots[j]=True
                jobs_sequence[j] = i[0]
                profit+= i[2]
                break           #it is necessary to break out of loop once the job is allocated, else every other slot is alloted for same job
    
    
    for k in jobs_sequence:
        print(k,"-->",end="")

    print("\n profit==",profit)



def sortJobs(jobs):
    n = len(jobs)
    for i in range(n):
        for j in range(i+1,n):
            if(jobs[i][2] < jobs[j][2]):
                temp = jobs[i]
                jobs[i] = jobs[j]
                jobs[j] = temp

    return jobs


def maximum(jobs):
    max = -1
    for i in jobs:
        if(i[1]>max):
            max = i[1]
    
    return max

jobs = [
        
        ['a',3,40],
        ['b',2,10],       
        ['d',1,5],
        ['e',2,40],
        ['c',3,60],
        
]

jobs = sortJobs(jobs)  #in decreasing order of profits
n = maximum(jobs)
sequenceJobs(jobs,n)